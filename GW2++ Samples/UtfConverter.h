// original code is from here: http://www.codeproject.com/KB/string/UtfConverter.aspx
// License: The Code Project Open License (CPOL) http://www.codeproject.com/info/cpol10.aspx

#ifndef UTFCONVERTER__H__
#define UTFCONVERTER__H__

namespace UtfConverter
{
	std::wstring FromUtf8(const std::string& utf8string);
	std::string ToUtf8(const std::wstring& widestring);
}

#endif
