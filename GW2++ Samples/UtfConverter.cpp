// Code originally from: http://www.codeproject.com/KB/string/UtfConverter.aspx
// License: The Code Project Open License (CPOL), not OSI-recognized.
// http://www.codeproject.com/info/cpol10.aspx

// (2013-06-10)
// - clearer comments (CPOL requires explicit change long)
// - useful exception messages
// - initialize std::wstring directly
// - general refactoring

// Changes by made to the original code (14/3/2011):
// 1. replaced #include "stdafx.h" with #include <string>
// 2. replaced "return resultstring;" with "return resultstring.c_str();" - it fixes a string length issue

#include <string>
#include <stdexcept>

#ifndef UTFCONVERTER__H__
#include "UtfConverter.h"
#endif

#ifndef CONVERTUTF__H__
#include "ConvertUTF.h"
#endif

namespace UtfConverter
{
	std::wstring FromUtf8(const std::string &utf8string)
	{
		size_t widesize = utf8string.length();
        std::wstring resultstring(widesize + 1, L'\0');
        const UTF8* sourcestart = reinterpret_cast<const UTF8*>(utf8string.c_str());
        const UTF8* sourceend = sourcestart + widesize;

        if (sizeof(wchar_t) == 2)
		{
			UTF16* targetstart = reinterpret_cast<UTF16*>(&resultstring[0]);
			UTF16* targetend = targetstart + widesize;
			ConversionResult res = ConvertUTF8toUTF16(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
			if (res != conversionOK)
			{
				throw std::exception("UTF-8 to UTF-16 conversion failed.");
			}
			*targetstart = 0;
            return resultstring.c_str();
		}
		else if (sizeof(wchar_t) == 4)
		{
			UTF32* targetstart = reinterpret_cast<UTF32*>(&resultstring[0]);
			UTF32* targetend = targetstart + widesize;
			ConversionResult res = ConvertUTF8toUTF32(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
			if (res != conversionOK)
			{
				throw std::exception("UTF-8 to UTF-32 conversion failed.");
			}
			*targetstart = 0;
			return resultstring.c_str();
		}
		else
		{
            throw std::exception("sizeof(wchar_t) expected to be 2 or 4.");
		}
		return L"";
	}

	std::string ToUtf8(const std::wstring& widestring)
	{
		size_t widesize = widestring.length();

		if (sizeof(wchar_t) == 2)
		{
			size_t utf8size = 3 * widesize + 1;
			std::string resultstring(utf8size, '\0');
			const UTF16* sourcestart = reinterpret_cast<const UTF16*>(widestring.c_str());
			const UTF16* sourceend = sourcestart + widesize;
			UTF8* targetstart = reinterpret_cast<UTF8*>(&resultstring[0]);
			UTF8* targetend = targetstart + utf8size;
			ConversionResult res = ConvertUTF16toUTF8(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
			if (res != conversionOK)
			{
				throw std::exception("UTF-16 to UTF-8 conversion failed.");
			}
			*targetstart = 0;
			return resultstring.c_str();
		}
		else if (sizeof(wchar_t) == 4)
		{
			size_t utf8size = 4 * widesize + 1;
			std::string resultstring(utf8size, '\0');
			const UTF32* sourcestart = reinterpret_cast<const UTF32*>(widestring.c_str());
			const UTF32* sourceend = sourcestart + widesize;
			UTF8* targetstart = reinterpret_cast<UTF8*>(&resultstring[0]);
			UTF8* targetend = targetstart + utf8size;
			ConversionResult res = ConvertUTF32toUTF8(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
			if (res != conversionOK)
			{
				throw std::exception("UTF-32 to UTF-8 conversion failed.");
			}
			*targetstart = 0;
			return resultstring.c_str();
		}
		else
		{
            throw std::exception("sizeof(wchar_t) expected to be 2 or 4.");
		}
		return "";
	}
}
