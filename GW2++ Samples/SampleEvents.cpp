#ifndef GW2_SAMPLEEVENTS_H
#include "SampleEvents.h"
#endif

#ifndef GW2_EVENTS_H
#include "Events.h"
#endif

#include <iostream>

void events()
{
    // Read SSL certificate file. The contents of this file are identical to
    // the in-memory certificate.
    gw2::Certificate cert("../gw2api.pem");

    // Pass certificate file and use default English language.
    gw2::api::Events events(cert);
    
    // Fetch only the events in map #873 on world #1001. The order of these is
    // irrelevant. Unrecognized query arguments are simply ignored (i.e.
    // passing ItemId would have no effect). Arguments may occur multiple times
    // in which case behaviour is defined by the server's response. At the time
    // of writing the first instance of any argument is used, others are
    // ignored. See documentation for gw2::Query for details.
    gw2::Query q(gw2::Query::Argument::WorldId, gw2::WorldId(1001));
    q += gw2::Query(gw2::Query::Argument::MapId, gw2::MapId(873));

    // This wouldn't break anything but the result becomes defined by the
    // server and thus unpredictable.
    //q += gw2::Query(gw2::Query::Argument::MapId, gw2::MapId(73));

    // Pass the query to events.
    events.set_query(q);

    // Fetch results.
    events.refresh();
    auto data(events.data());

    // Print out the results.
    for (auto beg = data.cbegin(), end = data.cend(); beg != end; ++beg)
    {
        // NB: The name is set to the empty string at this point, since it has
        // to be gotten via the EventNames API.
        std::cout << "Event ID: " << beg->id()
            << "\n\tName: '" << beg->name()
            << "'\n\tWorld ID: " << beg->world_id()
            << "\n\tMap ID: " << beg->map_id()
            << "\n\tState: " << gw2::state_to_str(beg->state())
            << std::endl;
    }
}