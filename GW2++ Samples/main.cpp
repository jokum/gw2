#ifndef GW2_SAMPLEWORLDNAMES_H
#include "SampleWorldNames.h"
#endif

#ifndef GW2_SAMPLEMAPNAMES_H
#include "SampleMapNames.h"
#endif

#ifndef GW2_SAMPLEEVENTS_H
#include "SampleEvents.h"
#endif

#ifndef GW2_SAMPLEEVENTNAMES_H
#include "SampleEventNames.h"
#endif

#ifndef GW2_SAMPLEWVWMATCHES_H
#include "SampleWvWMatches.h"
#endif

#include <iostream>

int main()
{
    std::cout << "World name sample." << std::endl;
    world_names();

    std::cout << "\nMap name sample." << std::endl;
    map_names();

    std::cout << "\nEvent state name sample." << std::endl;
    events();
    
    std::cout << "\nEvent name and -state sample." << std::endl;
    //event_names();
    
    std::cout << "\nWvW matches sample." << std::endl;
    wvw_matches();

    std::cin.get();
    return 0;
}
