#ifndef GW2_SAMPLEWORLDNAMES_H
#include "SampleWorldNames.h"
#endif

#ifndef GW2_WORLDNAMES_H
#include "WorldNames.h"
#endif

#include <iostream>

#ifndef UTFCONVERTER__H__
#include "UtfConverter.h"
#endif

void world_names()
{
    // Use in-memory SSL certificate (default constructor).
    gw2::Certificate cert;

    // Get world names in Spanish.
    gw2::api::WorldNames world_names(cert, gw2::api::Language::Spanish);

    // Fetch results.
    world_names.refresh();

    // Copy the results locally so we no longer depend on world_names.
    std::unordered_map<gw2::WorldId, std::string> data(world_names.data());

    // Print out the results. NB: Spanish names include non-ASCII characters.
    // UTF-8, the actual text encoding, isn't default on Windows so it is
    // necessary to first widen the characters. This project includes sample
    // code for that pupose (as illustrated below) but any means of widening
    // text should work. On many Linux systems std::string is already UTF-8.
    // See readme for further details.
    for (auto beg = data.cbegin(), end = data.cend(); beg != end; ++beg)
    {
        std::wstring ws(UtfConverter::FromUtf8(beg->second));

        std::wcout << "World ID: " << beg->first.str().c_str() << "\tName: "
            << ws << std::endl;
    }
}