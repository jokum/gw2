#ifndef GW2_SAMPLEMAPNAMES_H
#include "SampleMapNames.h"
#endif

#include <algorithm>
#include <iostream>

void map_names()
{
    // Use in-memory SSL certificate and English language.
    gw2::api::MapNames map_names;

    // Fetch results.
    map_names.refresh();

    auto data(map_names.data());
    std::for_each(data.cbegin(), data.cend(), [](const std::pair<gw2::MapId, std::string> &map)
    {
        std::cout << "Map ID: " << map.first << "\tName: " << map.second << std::endl;
    });
}