#ifndef GW2_SAMPLEWVWMATCHES_H
#include "SampleWvWMatches.h"
#endif

#ifndef GW2_WVWMATCHES_H
#include "WvWMatches.h"
#endif

#include <iostream>

void wvw_matches()
{
    // In-memory certificate. The WvW matches API does not support localization
    // so the constructors do not take a language argument.
    gw2::api::WvWMatches matches;

    // Fetch results.
    matches.refresh();

    // Copy the results locally so we no longer depend on matches.
    auto data(matches.data());

    // Print out the results.
    for (auto beg = data.cbegin(), end = data.cend(); beg != end; ++beg)
    {
        std::cout << "Match ID: " << beg->id()
            << "\tRed world ID: " << beg->red_world()
            << "\tStart time: " << beg->start()
            << std::endl;
    }
}