#ifndef GW2_SAMPLEEVENTNAMES_H
#include "SampleEventNames.h"
#endif

#ifndef GW2_EVENTNAMES_H
#include "EventNames.h"
#endif

#ifndef GW2_WORLDNAMES_H
#include "WorldNames.h"
#endif

#ifndef GW2_MAPNAMES_H
#include "MapNames.h"
#endif

#ifndef GW2_EVENTS_H
#include "Events.h"
#endif

#include <iostream>

void event_names()
{
    // In-memory SSL certificate and English language.
    gw2::api::EventNames event_names(gw2::api::Language::English);
    
    // In-memory SSL certificate and French language.
    // You can mix APIs across languages however you want. However, you cannot
    // change the language of a created API.
    gw2::api::WorldNames world_names(gw2::api::Language::French);

    // In-memory SSL certificate and English language.
    gw2::api::MapNames map_names;

    // In-memory SSL certificate and *no* language. The language argument has
    // no meaning to this API so the constructors do not accept it.
    // Although events and event_names use different SSL certificates they are
    // read from the same block of memory so there is no excessive waste.
    // However, for multiple APIs it may still be a good idea to reuse the same
    // certificate.
    gw2::api::Events events;

    // Fetch only the event identified by the ID on world #1001.
    gw2::Query q(gw2::Query::Argument::WorldId, gw2::WorldId(1001));
    q += gw2::Query(gw2::Query::Argument::EventId, gw2::EventId("72F93CD8-94AC-4234-8D86-996CCAC76A46"));

    // Fetch results and copy to local variables, freeing up the APIs for
    // further use.
    events.set_query(q);
    events.refresh();
    auto event_res(events.data());
    
    event_names.refresh();
    auto event_names_res(event_names.data());
    
    world_names.refresh();
    auto world_names_res(world_names.data());

    map_names.refresh();
    auto map_names_res(map_names.data());

    // For every returned event (there should only be one) look up the name
    // from event_names_res and assign it. Note the use of non-const iterators.
    for (auto beg = event_res.begin(), end = event_res.end(); beg != end; ++beg)
    {
        beg->name(event_names_res.at(beg->id()));
    }

    // Print out the results. Note const iterators.
    for (auto beg = event_res.cbegin(), end = event_res.cend(); beg != end; ++beg)
    {
        // NB: The name is set to the empty string at this point, since it has
        // to be gotten via the EventNames API.
        std::cout << "Event ID: " << beg->id()
            << "\n\tName: " << beg->name()
            << "\n\tWorld name: " << world_names_res.at(beg->world_id())
            << "\n\tMap name: " << map_names_res.at(beg->map_id())
            << "\n\tState: " << gw2::state_to_str(beg->state())
            << std::endl;
    }
}