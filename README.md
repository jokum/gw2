# GW2++: Guild Wars 2 API Wrapper

This is a C++11 wrapper for [the official Guild Wars 2 API][gw2api] released in
May 2013.

The interface is not guaranteed to be completely stable at this point but if
any changes are made it will be to wrap some uses of `WorldId` and `MapId` in
proper world and map containers with greater functionality, just as `Event`
contains an `EventId`. The general interface is stable.

## Usage

Check out the GW2++ Samples project. All wrappers follow this pattern:

* Construct a wrapper instance, potentially specifying some localization
    language
* Filter the API with `Query` objects passed to the wrapper instance with
    `set_query(const Query &q)`
* Call `refresh()` to fetch data from the API and wait for processing to
    complete
* Copy transformed data with `data()` so the wrapper instance is free to be
    reused
    *  This is good practice, however, if you know you're not going to reuse a
        wrapper then you don't strictly speaking need to

The project is 32-bit and set to build as a static library.

## API Support

* Events
* Event names
* Map names
* World names
* WvW matches

Beta APIs will never be supported.

## Dependencies

This project is built on Windows, with VS2010, for Windows, but the GW2++ code
is platform agnostic. Porting the project to a *nix system should be reasonably
straight-forward. A number of C++11 features have been used so the equivalent of
at least VS2010 is required to build out of the box. At the time of writing
basically every mainstream compiler has greater C++11 support than VS2010's so
that shouldn't be a problem.

The project is built with the following and may or may not work with later
versions:

* [libcurl 7.19.3][libcurl] with SSL support
* [libJSON 7.6.1][libjson], already included in project

It requires the following DLLs:

* curllib.dll
* libeay32.dll
* openldap.dll
* ssleay32.dll
* [libsasl.dll][libsasl]

libsasl.dll belongs to [OpenLDAP for Windows][openldap] and is not included with
libcurl, though openldap.dll seems to be.

## License

libJSON is released under [a BSD 2-clause license][bsd2clause], which in some
edge cases can be slightly restrictive. The source files do not include a
license notice.

GW2++ itself is released under [the MIT Expat license][mit], which means you can do
pretty much whatever you want with it. The source files do not include a license
notice.

GW2++ Samples is likewise released under the above MIT license but for convenience
it includes source files released under the non-OSI [CPOL][cpol], a generally
permissive wall-of-text license. See the Encoding header for details.

## Non-features

These are "features" there are no plans to support. Some of these may change in
the future but it is unlikely and definitely won't happen in the foreseeable
future.

**This library has no native support for local caching.** I can't know how your
cache is stored and I'm not going to shove my personal preferences down anyone's
throat. Besides, if you have a cache, using a wrapper instance is almost
certainly less efficient than transforming the data into a format that can
interoperate with this library yourself. It would also be nonsensical seeing as
the wrappers mainly automate data fetching and parsing, both of which will
require special handling when working with caches.

**There are no plans to implement asynchrony.** I've been too lazy to implement
multi-threading and VS2010 doesn't support C++11's `<thread>`. You can probably
wrap calls to `refresh()` in asynchronous call without too much trouble.

## Project Status

This project isn't exactly actively maintained. It was started as a way to brush
up on C++ and exists solely for my own amusement, not because I, personally,
have a use case for it (I don't). The API will be supported to the extent that I
am interested in working on the project and I don't guarantee to ever support
all of it. I do guarantee to never support beta APIs. This is partly because C++
is a slow language to develop in, partly because I have limited personal
investment in the project.

## Encoding

Sensibly, the GW2 API returns UTF-8 data. UTF-8 is not ASCII. In the general case
this is pretty much irrelevant because the English localization is not likely to
(but might!) use non-ASCII characters, meaning they will render fine. For the other
localizations it is definitely a potential problem.

`std::string` and `std::wstring` are not arrays of letters. They are byte streams.
This is important. It means that I can freely pass around `std::string` internally
and leave encoding issues to the implementer and this is what I have done.
Specifically, GW2++ uses only `std::string` and libJSON is compiled with the unicode
setting turned off (UTF-8 and unicode are not interchangeable but that is irrelevant
here). In cases where output requires explicit UTF-8 conversions the implementer is
responsible for this -- neither libcurl, libJSON, nor GW2++ have any notion of data
encoding. To this end the GW2++ Samples project includes two headers and their
implementations facilitating conversion between UTF-8 and UTF-16/UTF-32. These files
are necessary to compile GW2++ Samples, obviously, but GW2++ itself does not need
them and can be used without any conversion at all. Note that these files are
released under a separate license, as per the License header.

On Linux systems `std::string` is often internally UTF-8 and so no conversion is
required and GW2++ can be used directly as-is. This isn't a guarantee, though, and
there are probably exceptions. On Windows `std::string` is UTF-16, meaning
conversion *is* required when special characters are involved. `std::string`s
returned by GW2++ must be transformed and stored in `std::wstring`s. Various samples
in the GW2++ Samples show one way to achieve this.

More detailed information on [`std::string` versus `std::wstring`][string].

[gw2api]: https://forum-en.guildwars2.com/forum/community/api/API-Documentation
[libcurl]: http://curl.haxx.se/download.html
[libjson]: http://sourceforge.net/projects/libjson/
[libsasl]: http://theetrain.ca/tech/files/libsasl.dll
[openldap]: http://www.userbooster.de/en/download/openldap-for-windows.aspx
[mit]: http://opensource.org/licenses/MIT
[bsd2clause]: http://opensource.org/licenses/BSD-2-Clause
[cpol]: http://www.codeproject.com/info/cpol10.aspx
[string]: http://stackoverflow.com/a/402918/482758