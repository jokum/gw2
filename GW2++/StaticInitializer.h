#ifndef GW2_STATICINITIALIZER_H
#define GW2_STATICINITIALIZER_H

namespace gw2
{
    /// This is a utility class responsible for run-once code to be executed at
    /// program launch and termination. Such code is run in the constructor
    /// respectively the destructor of this class. This class is not a
    /// Singleton -- multiple instances can be created and there is no special
    /// means of accessing any one of them (i.e. a static function) -- however,
    /// code will only be run once. The contructor and destructor make sure to
    /// do nothing when code has already been run, in case of multiple
    /// instantiations. In other words, the class behaves like a Singleton, but
    /// since it is not meant to supply any runtime funcionality there is no
    /// Singleton handle.
    class StaticInitializer
    {
    public:
        /// Invokes code that should only be run a single time during program
        /// setup. Multiple instantiations will not cause multiple code
        /// executions. However, if there is an error during code execution
        /// the instance will be left thinking it did not initialize properly
        /// and the constructor can be run again in its entirety.
        StaticInitializer();

        /// Invokes code that should only be run a single time during program
        /// termination. Multiple instantiations will not cause multiple code
        /// executions.
        ~StaticInitializer();

    private:
        static bool initialized;
        static bool cleaned;
    };

}
#endif // GW2_STATICINITIALIZER_H