#ifndef GW2_EVENTNAMES_h
#include "EventNames.h"
#endif

#ifndef JSON_OPTIONS_H
#include "../libjson/JSONOptions.h"
#endif

#ifndef LIBJSON_H
#include "../libjson/libjson.h"
#endif

#include <iostream>

namespace gw2
{
    namespace api
    {
        EventNames::EventNames(const api::Language::Name language)
            : ApiBase(api::Path::EventNames, language) {}

        EventNames::EventNames(const Certificate &cert, const api::Language::Name language)
            : ApiBase(api::Path::EventNames, cert, language) {}

        const std::unordered_map<EventId, std::string> EventNames::data() const
        {
            return event_names;
        }

        void  EventNames::add_node(const JSONNode &node)
        {
            try
            {
                EventId event_id(node.at("id").as_string());
                std::string event_name = node.at("name").as_string();

                event_names.insert(std::make_pair(event_id, event_name));
            }
            catch (std::out_of_range)
            {
                std::cerr << "Failed to add node a node."
                    << "Expected attribute not found." << std::endl;
            }
        }
    }
}