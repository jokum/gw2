#ifndef GW2_WVWMATCHES_H
#include "WvWMatches.h"
#endif

#ifndef JSON_OPTIONS_H
#include "../libjson/JSONOptions.h"
#endif

#ifndef LIBJSON_H
#include "../libjson/libjson.h"
#endif

#include <iostream>

namespace gw2
{
    namespace api
    {
        WvWMatches::WvWMatches()
            : ApiBase(api::Path::WvWMatches, api::Language::English) {}

        WvWMatches::WvWMatches(const Certificate &cert)
            : ApiBase(api::Path::WvWMatches, cert, api::Language::English) {}

        const std::vector<WvWMatch> WvWMatches::data() const
        {
            return wvw_matches;
        }

        void  WvWMatches::add_node(const JSONNode &node) 
        {
            try
            {
                WvWMatch match(WvWMatchId(node.at("wvw_match_id").as_string()));
                match.red_world(WorldId(node.at("red_world_id").as_int()));
                match.blue_world(WorldId(node.at("blue_world_id").as_int()));
                match.green_world(WorldId(node.at("green_world_id").as_int()));
                match.start(node.at("start_time").as_string());
                match.end(node.at("end_time").as_string());

                wvw_matches.push_back(match);
            }
            catch (std::out_of_range &e)
            {
                std::cerr << "Failed to add node: " << node.as_string()
                    << ", " << e.what() << std::endl;
            }
        }
    }
}
