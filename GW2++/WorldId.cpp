#ifndef GW2_WORLDID_H
#include "WorldId.h"
#endif

#include <sstream>
#include <functional>

namespace gw2
{
    const std::string WorldId::str() const
    {
        std::stringstream s;
        s << id;
        return s.str();
    }

    const std::string MapId::str() const
    {
        std::stringstream s;
        s << id;
        return s.str();
    }
}


namespace std
{
    size_t hash<gw2::MapId>::operator()(const gw2::MapId &m) const
    {
        return hash<int>()(m.value());
    }

    size_t hash<gw2::WorldId>::operator()(const gw2::WorldId &w) const
    {
        return hash<int>()(w.value());
    }

    size_t hash<gw2::EventId>::operator()(const gw2::EventId &e) const
    {
        return hash<std::string>()(e.value());
    }

    size_t hash<gw2::WvWMatchId>::operator()(const gw2::WvWMatchId &m) const
    {
        return hash<std::string>()(m.value());
    }
}
