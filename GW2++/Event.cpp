#ifndef GW2_EVENT_H
#include "Event.h"
#endif

#include <unordered_map>

namespace gw2
{
    namespace
    {
        /// std::hash<const std::string> is not defined. Override the hasher
        /// with non-const string.
        typedef std::unordered_map<const std::string, const Event::State, std::hash<std::string>> str_state_map_t;
        typedef std::unordered_map<const Event::State, const std::string> state_str_map_t;

        const str_state_map_t create_str_map()
        {
            typedef str_state_map_t::value_type str_to_state_t;
            str_state_map_t m;
            m.emplace(str_to_state_t("Active", Event::State::Active));
            m.emplace(str_to_state_t("Fail", Event::State::Fail));
            m.emplace(str_to_state_t("Preparation", Event::State::Preparation));
            m.emplace(str_to_state_t("Success", Event::State::Success));
            m.emplace(str_to_state_t("Warmup", Event::State::Warmup));
            return m;
        }

        const state_str_map_t create_state_map()
        {
            typedef state_str_map_t::value_type state_to_str_t;
            state_str_map_t m;
            m.emplace(state_to_str_t(Event::State::Active, "Active"));
            m.emplace(state_to_str_t(Event::State::Fail, "Fail"));
            m.emplace(state_to_str_t(Event::State::Inactive, "Inactive"));
            m.emplace(state_to_str_t(Event::State::Preparation, "Preparation"));
            m.emplace(state_to_str_t(Event::State::Success, "Success"));
            m.emplace(state_to_str_t(Event::State::Warmup, "Warmup"));
            return m;
        }

        const str_state_map_t str_to_state_map(create_str_map());
        const state_str_map_t state_to_str_map(create_state_map());
    }

    // Everything above this comment is not declared in Event.h and has
    // hidden scope. Everything below is and has visible scope.

    Event::Event(const EventId &event_id, State event_state, WorldId world_id, MapId map_id)
        : m_id(event_id)
        , m_name()
        , m_state(event_state)
        , m_world_id(world_id)
        , m_map_id(map_id)
    {}

    Event::Event(const EventId &event_id, const std::string &event_name, State event_state, WorldId world_id, MapId map_id)
        : m_id(event_id)
        , m_name(event_name)
        , m_state(event_state)
        , m_world_id(world_id)
        , m_map_id(map_id)
    {}

    const Event::State str_to_state(const std::string &s)
    {
        return str_to_state_map.at(s);
    }

    const std::string state_to_str(const Event::State state)
    {
        return state_to_str_map.at(state);
    }
}