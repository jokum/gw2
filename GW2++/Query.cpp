#ifndef GW2_QUERY_H
#include "Query.h"
#endif

#include <algorithm>
#include <cassert>
#include <sstream>
#include <unordered_map>

namespace gw2
{
    namespace
    {
        typedef std::unordered_map<const Query::Argument, const std::string> query_map_t;

        const query_map_t create_query_arg_map();
        const query_map_t query_args(create_query_arg_map());

        const query_map_t create_query_arg_map()
        {
            typedef query_map_t::value_type query_arg_t;
            query_map_t m;
            m.emplace(query_arg_t(Query::Argument::EventId, "event_id"));
            m.emplace(query_arg_t(Query::Argument::ItemId, "item_id"));
            m.emplace(query_arg_t(Query::Argument::MapId, "map_id"));
            m.emplace(query_arg_t(Query::Argument::MatchId, "match_id"));
            m.emplace(query_arg_t(Query::Argument::RecipeId, "recipe_id"));
            m.emplace(query_arg_t(Query::Argument::WorldId, "world_id"));
            return m;
        }
    }

    Query::Query(const Query::Argument query, const IdBase &value): queries()
    {
        assert(query >= Query::Argument::EventId && query <= Query::Argument::WorldId);
        queries.push_back(std::make_pair(query, value.str()));
    }

    const std::string Query::str() const
    {
        std::stringstream ss;
        std::for_each(queries.cbegin(), queries.cend(),
            [&ss](const std::pair<Query::Argument, std::string> &q)
        {
            ss << "&" + query_args.at(q.first) + "=" + q.second;
        });
        return ss.str();
    }

    Query& Query::operator+=(const Query &rhs)
    {
        queries.insert(queries.cend(), rhs.queries.cbegin(), rhs.queries.cend());
        return *this;
    }

    Query operator+(const Query &lhs, const Query &rhs)
    {
        Query q = lhs;
        q += rhs;
        return q;
    }
}