#ifndef GW2_CURLEASY_H
#define GW2_CURLEASY_H

#ifndef __CURL_CURL_H
#include "curl/curl.h"
#endif

#include <memory>
#include <string>

namespace gw2
{
    class Certificate; // Forward declaration.

    /// This class is a very simple wrapper for the curl_easy interface,
    /// providing only the few options required by GW2++. The CURL handle is
    /// stored in a C++11 shared_ptr.
    class CurlEasy
    {
    public:
        /// Constructs an uninitialized CURL handle. Before this handle can be
        /// used <see cref="init"/> must have been called and returned true.
        CurlEasy(): phandle(), code(CURLE_OK) {}

        /// Allocates a new CURL handle. This is the first function to call
        /// during any operation.
        /// <returns>True if all internal operators return
        /// <c>CURLE_OK</c></returns>
        bool init();

        /// Establishes connection and receives data.
        /// <returns>True if all internal operators return
        /// <c>CURLE_OK</c></returns>
        bool perform();

        /// Tells this handle to write incoming data in <see cref="perform"/>
        /// to <c>s</c>. If this function isn't called data will be printed to
        /// the default output (std::cout). This function both sets the write
        /// function to use and the destination (<c>s</c>) to write to.
        /// <returns>True if all internal operators return
        /// <c>CURLE_OK</c></returns>
        bool write_to(const std::string &s);

        /// Tells this handle to perform SSL peer verification when available.
        /// The handle uses the certificate passed in
        /// <see cref="certify_with"/>.
        /// <returns>True if all internal operators return
        /// <c>CURLE_OK</c></returns>
        /// <seealso cref="verify_host"/>
        /// <seealso cref="certify_with"/>
        bool verify_peer(const bool verify = true);

        /// Tells this handle to perform SSL host verification when available.
        /// The handle uses the certificate passed in
        /// <see cref="certify_with"/>.
        /// <returns>True if all internal operators return
        /// <c>CURLE_OK</c></returns>
        /// <seealso cref="verify_peer"/>
        /// <seealso cref="certify_with"/>
        bool verify_host(const bool verify = true);

        /// Tells this handle to use the given <see cref="Certificate"/> to
        /// authenticate the connection.
        /// <param name="cert">The certificate to authenticate with</param>
        /// <returns>True if all internal operators return
        /// <c>CURLE_OK</c></returns>
        /// <seealso cref="verify_peer"/>
        /// <seealso cref="verify_host"/>
        bool certify_with(const Certificate &cert);

        /// Returns the human-readable error/info message of the current state
        /// of this handle.
        /// <returns>True if all internal operators return
        /// <c>CURLE_OK</c></returns>
        const std::string error() const;

        /// Sets the URL for this handle to connect to.
        /// <param name="s">The URL to connect to.</param>
        /// <returns>True if all internal operators return
        /// <c>CURLE_OK</c></returns>
        bool url(const std::string &s);

        /// Cleans up this handle. This is the last function to call during an
        /// operation.
        void cleanup();

    private:
        std::shared_ptr<CURL * const> phandle;
        CURLcode code;
    };
}
#endif // GW2_CURLWRAP_H