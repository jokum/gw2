#ifndef GW2_QUERY_H
#define GW2_QUERY_H

#ifndef GW2_WORLDID_H
#include "WorldId.h"
#endif

#include <string>
#include <vector>

namespace gw2
{
    /// This class represents a set of URL query arguments with values.
    /// Instances of this class can be passed as arguments to
    /// <see cref="ApiBase::set_query"/> to apply filters to API wrapper
    /// instances. Note that although the API language is technically
    /// implemented as a query argument it is not made available as one here.
    /// This is because API wrappers are built up around the chosen language,
    /// meaning if you need a different language you create a new instance, on
    /// the basis that changing language during runtime is nonsensical.
    ///
    /// Instances of this class store a vector of pairs of arguments and values
    /// and may contain multiple identical arguments with same or different
    /// values. If that happens the behaviour is left to the server. This is
    /// because <c>operator+</c> cannot be sensibly defined for containers that
    /// can only contain one instance of each argument (do you override
    /// previous values? Left- or right-hand precedence? etc.).
    /// <seealso cref="ApiBase"/>
    /// <seealso cref="IdBase"/>
    class Query
    {
    public:
        /// The available query arguments. API Language is not treated as a
        /// query argument since API wrappers are built up around them.
        /// Arguments are in alphabetical order.
        enum Argument
        {
            EventId,
            ItemId,
            MapId,
            MatchId,
            RecipeId,
            WorldId
        };

        /// Constructs an empty query that does nothing.
        Query(): queries() {}

        /// Constructs a query from the supplied arguments.
        /// <param name="query">The <see cref="Argument"/> type.</param>
        /// <param name="val">The value of <c>query</c>.</param>
        Query(const Query::Argument query, const IdBase &val);

        /// Generates a query string representation of queries.
        /// <returns>The query string representation of this query
        /// object.</returns>
        const std::string str() const;

        /// Adds the contents of the right-hand argument to the left-hand
        /// argument.
        /// <param name="rhs">The right-hand argument to add to the left-hand
        /// argument.</param>
        /// <returns>The left-hand argument with the right-hand queries added
        /// to it.</returns>
        Query& operator+=(const Query &rhs);

    private:
        std::vector<std::pair<Query::Argument, std::string>> queries;
    };

    /// Adds the contents of the left- and right-hand arguments in a new Query
    /// object.
    /// <param name="lhs">The left-hand argument.</param>
    /// <param name="rhs">The right-hand argument.</param>
    /// <returns>The union of <c>lhs</c> and <c>rhs</c> in a new
    /// Query instance.</returns>
    Query operator+(const Query &lhs, const Query &rhs);
}
#endif // GW2_QUERY_H