#ifndef GW2_MAPNAMES_H
#include "MapNames.h"
#endif

#ifndef JSON_OPTIONS_H
#include "../libjson/JSONOptions.h"
#endif

#ifndef LIBJSON_H
#include "../libjson/libjson.h"
#endif

#include <iostream>

namespace gw2
{
    namespace api
    {
        MapNames::MapNames(const api::Language::Name language)
            : ApiBase(api::Path::MapNames, language) {}

        MapNames::MapNames(const Certificate &cert, const api::Language::Name language)
            : ApiBase(api::Path::MapNames, cert, language) {}

        const std::unordered_map<MapId, std::string> MapNames::data() const
        {
            return map_names;
        }

        void  MapNames::add_node(const JSONNode &node) 
        {
            try
            {
                MapId id(std::stoi(node.at("id").as_string()));
                std::string name = node.at("name").as_string();

                map_names.insert(std::make_pair(id, name));
            }
            catch (std::logic_error &e)
            {
                // at() can throw out_of_range.
                // stoi() can throw out_of_range or invalid_argument.
                // There's no reason to treat them differently.
                std::cerr << "Failed to add node: " << node.as_string()
                    << ", " << e.what() << std::endl;
            }
        }
    }
}