#ifndef GW2_CERTIFICATE_H
#define GW2_CERTIFICATE_H

#ifndef __CURL_CURL_H
#include "curl/curl.h"
#endif

#include <string>

namespace gw2
{
    class Certificate
    {
    public:
        // Construct an in-memory certificate for certifying GW2 API SSL
        // connections.
        Certificate(): in_memory(true), cert_file() {}

        // Read a GW2 API SSL certificate stored in PEM format at the given
        // location. The file must remain valid for the lifetime of this
        // object. The file may contain several certificates and only one is
        // required to match the API certificate.
        Certificate(const std::string &cert_file_path)
            : in_memory(false), cert_file(cert_file_path) {}

        // Invoke this object to certify the CURL* argument.
        CURLcode operator()(CURL * const curl) const;

    private:
        bool in_memory;
        std::string cert_file;
    };
}
#endif // GW2_CERTIFICATE_H