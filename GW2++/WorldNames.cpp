#ifndef GW2_WORLDNAMES_h
#include "WorldNames.h"
#endif

#ifndef JSON_OPTIONS_H
#include "../libjson/JSONOptions.h"
#endif

#ifndef LIBJSON_H
#include "../libjson/libjson.h"
#endif

#include <iostream>

namespace gw2
{
    namespace api
    {
        WorldNames::WorldNames(const api::Language::Name language)
            : ApiBase(api::Path::WorldNames, language) {}

        WorldNames::WorldNames(const Certificate &cert, const api::Language::Name language)
            : ApiBase(api::Path::WorldNames, cert, language) {}

        const std::unordered_map<WorldId, std::string> WorldNames::data() const
        {
            return world_names;
        }

        void  WorldNames::add_node(const JSONNode &node) 
        {
            try
            {
                WorldId world_id(std::stoi(node.at("id").as_string()));
                std::string world_name(node.at("name").as_string());

                world_names.insert(std::make_pair(world_id, world_name));
            }
            catch (std::logic_error &e)
            {
                // at() can throw out_of_range.
                // stoi() can throw out_of_range or invalid_argument.
                // There's no reason to treat them differently.
                std::cerr << "Failed to add node: " << node.as_string()
                    << ", " << e.what() << std::endl;
            }
        }
    }
}