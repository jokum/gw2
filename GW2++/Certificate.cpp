#ifndef GW2_CERTIFICATE_H
#include "Certificate.h"
#endif

#ifndef HEADER_SSL_H
#include "openssl/ssl.h"
#endif

namespace gw2
{
    namespace
    {
        /*
        This is the GeoTrust certificate used to authenticate the GW2 API
        calls. It is in PEM format, which is *not* what you get from
        exporting a certificate from e.g. Firefox. See following URLs:
        http://curl.haxx.se/docs/sslcerts.html for general info about this.
        http://curl.haxx.se/docs/caextract.html for PEM certificate bundles.
        */
        const std::string GeoTrustGlobal_cert(
            "-----BEGIN CERTIFICATE-----\n"\
            "MIIDVDCCAjygAwIBAgIDAjRWMA0GCSqGSIb3DQEBBQUAMEIxCzAJBgNVBAYTAlVTMRYwFAYDVQQK\n"\
            "Ew1HZW9UcnVzdCBJbmMuMRswGQYDVQQDExJHZW9UcnVzdCBHbG9iYWwgQ0EwHhcNMDIwNTIxMDQw\n"\
            "MDAwWhcNMjIwNTIxMDQwMDAwWjBCMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNR2VvVHJ1c3QgSW5j\n"\
            "LjEbMBkGA1UEAxMSR2VvVHJ1c3QgR2xvYmFsIENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB\n"\
            "CgKCAQEA2swYYzD99BcjGlZ+W988bDjkcbd4kdS8odhM+KhDtgPpTSEHCIjaWC9mOSm9BXiLnTjo\n"\
            "BbdqfnGk5sRgprDvgOSJKA+eJdbtg/OtppHHmMlCGDUUna2YRpIuT8rxh0PBFpVXLVDviS2Aelet\n"\
            "8u5fa9IAjbkU+BQVNdnARqN7csiRv8lVK83Qlz6cJmTM386DGXHKTubU1XupGc1V3sjs0l44U+Vc\n"\
            "T4wt/lAjNvxm5suOpDkZALeVAjmRCw7+OC7RHQWa9k0+bw8HHa8sHo9gOeL6NlMTOdReJivbPagU\n"\
            "vTLrGAMoUgRx5aszPeE4uwc2hGKceeoWMPRfwCvocWvk+QIDAQABo1MwUTAPBgNVHRMBAf8EBTAD\n"\
            "AQH/MB0GA1UdDgQWBBTAephojYn7qwVkDBF9qn1luMrMTjAfBgNVHSMEGDAWgBTAephojYn7qwVk\n"\
            "DBF9qn1luMrMTjANBgkqhkiG9w0BAQUFAAOCAQEANeMpauUvXVSOKVCUn5kaFOSPeCpilKInZ57Q\n"\
            "zxpeR+nBsqTP3UEaBU6bS+5Kb1VSsyShNwrrZHYqLizz/Tt1kL/6cdjHPTfStQWVYrmm3ok9Nns4\n"\
            "d0iXrKYgjy6myQzCsplFAMfOEVEiIuCl6rYVSAlk6l5PdPcFPseKUgzbFbS9bZvlxrFUaKnjaZC2\n"\
            "mqUPuLk/IH2uSrW4nOQdtqvmlKXBx4Ot2/Unhw4EbNX/3aBd7YdStysVAq45pmp06drE57xNNB6p\n"\
            "XE0zX5IJL4hmXXeXxx12E6nV5fEWCRE11azbJHFwLJhWC9kXtNHjUStedejV0NxPNO3CBWaAocvm\n"\
            "Mw==\n"\
            "-----END CERTIFICATE-----\n");

        /*
        Read an SSL certificate from memory. The certificate is hardcoded. This
        function is based on an official cURL example:
        http://curl.haxx.se/libcurl/c/cacertinmem.html
        Because the first and third arguments are not used they have been left
        unnamed.
        */
        CURLcode sslctx_function(CURL * const, void * const sslctx, void * const)
        {
            BIO * const bio = BIO_new_mem_buf(const_cast<char *>(GeoTrustGlobal_cert.c_str()),
                GeoTrustGlobal_cert.length());
            /*
            Read the PEM formatted certificate from memory into an X509 struct that
            SSL can use.
            */ 
            X509 * const cert = PEM_read_bio_X509(bio, nullptr, 0, nullptr);
            if (!cert)
            {
                //std::cerr << "PEM_read_bio_X509() failed." << std::endl;
                return CURLE_SSL_CERTPROBLEM;
            }

            X509_STORE * const store = SSL_CTX_get_cert_store(static_cast<const SSL_CTX *>(sslctx));
            if (X509_STORE_add_cert(store, cert) == 0)
            {
                //std::cerr << "Error adding certificate." << std::endl;
                return CURLE_SSL_CERTPROBLEM;
            }

            return CURLE_OK ;
        }
    }

    CURLcode Certificate::operator()(CURL * const curl) const
    {
        if (in_memory)
        {
            return curl_easy_setopt(curl, CURLOPT_SSL_CTX_FUNCTION,
                &sslctx_function);
        }
        return curl_easy_setopt(curl, CURLOPT_CAINFO, cert_file.c_str());
    }
}