#ifndef GW2_APIBASE_H
#include "ApiBase.h"
#endif

#ifndef GW2_STATICINITIALIZER_H
#include "StaticInitializer.h"
#endif

#ifndef LIBJSON_H
#include "../libjson/libjson.h"
#endif

#ifndef JSON_OPTIONS_H
#include "../libjson/JSONOptions.h"
#endif

#include <cassert>
#include <iostream>
#include <unordered_map>

using std::string;
using std::unordered_map;

namespace gw2
{
    namespace api
    {
        namespace
        {
            /// Sets up run-once code, to be run once at setup and once at
            /// tear-down.
            /// <seealso cref="StaticInitializer"/>.
            const StaticInitializer once_per_session;

            /// Helpers for constructing helper maps that should not be
            /// reachable outside this file.
            typedef unordered_map<const Path::Name, const string> api_map_t;
            typedef unordered_map<const Language::Name, const string> lang_map_t;

            /// api::Language::Name => std::string.
            const lang_map_t create_language_map()
            {
                typedef lang_map_t::value_type lang_t;
                lang_map_t m;
                m.emplace(lang_t(Language::English, "en"));
                m.emplace(lang_t(Language::French, "fr"));
                m.emplace(lang_t(Language::German, "de"));
                m.emplace(lang_t(Language::Spanish, "es"));
                return m;
            }

            /// std::string => api::Language::Name.
            const api_map_t create_api_map()
            {
                typedef api_map_t::value_type api_t;
                api_map_t m;

                // PvE.
                m.emplace(api_t(Path::Events, "events.json"));
                m.emplace(api_t(Path::MapNames, "map_names.json"));
                m.emplace(api_t(Path::EventNames, "event_names.json"));
                m.emplace(api_t(Path::WorldNames, "world_names.json"));

                // WvW.
                m.emplace(api_t(Path::WvWMatchDetails, "wvw/match_details.json"));
                m.emplace(api_t(Path::WvWMatches, "wvw/matches.json"));
                m.emplace(api_t(Path::WvWObjectives, "wvw/objective_names.json"));
                return m;
            }

            const lang_map_t languages(create_language_map());
            const api_map_t apis(create_api_map());
            const string api_url("https://api.guildwars2.com/v1/");
        }

        // Everything above this comment is not declared in ApiBase.h and has
        // hidden scope. Everything below is and has visible scope.

        const string url(const Path::Name name, const Language::Name lang)
        {
            assert(name >= Path::Events && name <= Path::WvWObjectives
                && lang >= Language::English && lang <= Language::Spanish);
            return api_url + apis.at(name) + "?lang=" + languages.at(lang);
        }

        void ApiBase::refresh()
        {
            // TODO: check return codes.
            if (c.init())
            {
                prepare_curl();
                // If json_response isn't cleared we will keep appending data
                // from subsequent calls.
                json_response.clear();
                if (!c.perform())
                {
                    std::cerr << "curl_easy_perform() failed: " << c.error() << std::endl;
                }
                else
                {
                    prepare_container();
                    // TODO: Make sure this is exception safe.
                    auto node = libjson::parse(json_response);
                    parse(node);
                }
                c.cleanup();
            }
        }

        void ApiBase::set_query(const Query &query)
        {
            q = query;
        }

        ApiBase::ApiBase(const Path::Name api, const Language::Name language)
            : lang(language)
            , certify()
            , base_url(api::url(api, language))
            , json_response()
            , q()
            , c()
        {}

        ApiBase::ApiBase(const Path::Name api, const Certificate &cert, const Language::Name language)
            : lang(language)
            , certify(cert)
            , base_url(api::url(api, language))
            , json_response()
            , q()
            , c()
        {}

        void ApiBase::prepare_curl()
        {
            // TODO: check return values.
            c.certify_with(certify);
            c.url(url());
            c.write_to(json_response);
            c.verify_host();
            c.verify_peer();
        }

        const string ApiBase::url() const
        {
            return base_url + q.str();
        }

        void ApiBase::parse(const JSONNode &node)
        {
            for (JSONNode::const_iterator beg = node.begin(), end = node.end();
                beg != end; ++beg)
            {
                if (beg->type() == JSON_NODE)
                {
                    const string node_name = beg->name();
                    if (node_name == "")
                    {
                        add_node(*beg);
                    }
                }
                else if (beg->type() == JSON_ARRAY)
                {
                    parse(*beg);
                }
            }
        }
    }
}