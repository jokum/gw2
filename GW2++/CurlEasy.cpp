#ifndef GW2_CURLEASY_H
#include "CurlEasy.h"
#endif

#ifndef GW2_CERTIFICATE_H
#include "Certificate.h"
#endif

namespace gw2
{
    // Hide this so declaring it elsewhere won't grant access to it.
    namespace
    {
        /// CurlEasy uses this function to write incoming data if
        /// <see cref="write_to"/> has been set. The function is exclusive to the
        /// current scope. The function is adapted from an official CURL example.
        /// <param name="ptr">A pointer to the start of the incoming data.</param>
        /// <param name="size">The size of one <c>nmemb</c>.</param>
        /// <param name="nmemb">The number of incoming characters.</param>
        /// <param name="userdata"/>A pointer to a string to which <c>ptr</c> can
        /// be appended.</param>
        /// <returns><c>size * nmemb</c>, indicating success. This function always
        /// returns that regardless of what happened.</returns>
        size_t writefunction(const char *ptr, size_t size, size_t nmemb, void *userdata)
        {
            std::string *s = static_cast<std::string *>(userdata);
            s->append(ptr, size * nmemb);
            return nmemb * size;
        }
    }

    bool CurlEasy::init()
    {
        phandle = std::make_shared<CURL *>(curl_easy_init());
        return *phandle != nullptr;
    }

    bool CurlEasy::perform()
    {
        code = curl_easy_perform(*phandle);
        return code == CURLE_OK;
    }

    bool CurlEasy::write_to(const std::string &s)
    {
        code = curl_easy_setopt(*phandle, CURLOPT_WRITEFUNCTION, &writefunction);
        code = curl_easy_setopt(*phandle, CURLOPT_WRITEDATA, &s);
        return code == CURLE_OK;
    }

    bool CurlEasy::verify_peer(const bool verify)
    {
        code = curl_easy_setopt(*phandle, CURLOPT_SSL_VERIFYPEER, (verify ? 1L : 0L));
        return code == CURLE_OK;
    }

    bool CurlEasy::verify_host(const bool verify)
    {
        // For hosts, 2 means verify, 0 means don't, and 1 is ignored!
        code = curl_easy_setopt(*phandle, CURLOPT_SSL_VERIFYHOST, (verify ? 2L : 0L));
        return code == CURLE_OK;
    }

    bool CurlEasy::certify_with(const Certificate & cert)
    {
        code = cert(*phandle);
        return code == CURLE_OK;
    }

    const std::string CurlEasy::error() const
    {
        return curl_easy_strerror(code);
    }

    bool CurlEasy::url(const std::string &s)
    {
        code = curl_easy_setopt(*phandle, CURLOPT_URL, s.c_str());
        return code == CURLE_OK;
    }

    void CurlEasy::cleanup()
    {
        curl_easy_cleanup(*phandle);
        phandle.reset();
    }
}