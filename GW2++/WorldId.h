#ifndef GW2_WORLDID_H
#define GW2_WORLDID_H

#include <string>

namespace gw2
{
    /// This base exists mainly for polymorphism. It makes passing arguments to
    /// <see cref="Query"/> easier. It is an abstract class with a single
    /// pure virtual function returning a string representation of deriving
    /// classes' IDs.
    /// Deriving classes are expected to implement a member function
    /// <c>T value()</c> returning the ID of the instance in the appropriate
    /// type and <c>void value(const T)</c> for altering the ID.
    class IdBase
    {
    public:
        /// Generates a string representation of this class' ID. IDs may be
        /// pure integers or GUIDs.
        /// <returns>A string representation of an ID.</returns>
        virtual const std::string str() const = 0;

    protected:
        IdBase() {}
    };

    /// This class represents a world ID. World IDs are four-digit integers of
    /// the form 1xxx or 2xxx. The former is for NA servers and the latter for
    /// EU servers.
    class WorldId : public IdBase
    {
    public:
        /// Constructs a new WorldId with value 0, which is invalid.
        explicit WorldId(void): id(0) {}

        /// Constructs a new WorldId from the argument, which is in no way
        /// verified. I.e. it is the user's responsibility to only create valid
        /// instances.
        /// <param name="world">The world ID this instance represents.</param>
        explicit WorldId(const int world): id(world) {}

        /// Returns the ID of this instance in its native type.
        /// <returns>The integer world ID this instance represents.</param>
        inline int value() const { return id; }

        /// Sets the ID of this instance.
        /// <param name="world">The world ID this instance should
        /// represent.</param>
        inline void value(const int world) { id = world; }

        const std::string str() const override;

    private:
        int id;
    };

    /// This class represents a map ID. Map IDs are integers with no pattern.
    class MapId : public IdBase
    {
    public:
        /// Constructs a new MapId with value 0, which is invalid.
        explicit MapId(void): id(0) {}

        /// Constructs a new MapId from the argument, which is in no way
        /// verified. I.e. it is the user's responsibility to only create valid
        /// instances.
        /// <param name="map">The map ID this instance represents.</param>
        explicit MapId(const int map): id(map) {}

        /// Returns the ID of this instance in its native type.
        /// <returns>The integer map ID this instance represents.</param>
        inline int value() const { return id; }

        /// Sets the ID of this instance.
        /// <param name="map">The map ID this instance should
        /// represent.</param>
        inline void value(const int map) { id = map; }

        const std::string str() const override;

    private:
        int id;
    };

    /// This class represents a dynamic event ID. Event IDs are GUIDs.
    class EventId : public IdBase
    {
    public:
        /// Constructs a new EventId set to the empty string, which is invalid.
        explicit EventId(void): id() {}

        /// Constructs a new EventId from the argument, which is in no way
        /// verified. I.e. it is the user's responsibility to only create valid
        /// instances.
        /// <param name="e">The event ID this instance represents.</param>
        explicit EventId(const std::string &e): id(e) {}

        /// Returns the ID of this instance in its native type.
        /// <returns>The string event ID this instance represents.</param>
        inline const std::string value() const { return id; }

        /// Sets the ID of this instance.
        /// <param name="e">The event ID this instance should
        /// represent.</param>
        inline void value(const std::string &e) { id = e; }

        const std::string str() const override { return id; }

    private:
        std::string id;
    };

    /// This class represents a WvW match ID. Match IDs are strings on the
    /// form "x-y" where x and y are single-digit integers.
    class WvWMatchId : public IdBase
    {
    public:
        /// Constructs a new WvWMatchId set to the empty string, which is
        /// invalid.
        explicit WvWMatchId(void): id() {}

        /// Constructs a new WvWMatchId from the argument, which is in no way
        /// verified. I.e. it is the user's responsibility to only create valid
        /// instances.
        /// <param name="m">The WvW match ID this instance represents.</param>
        explicit WvWMatchId(const std::string &m): id(m) {}

        /// Returns the ID of this instance in its native type.
        /// <returns>The string WvW match ID this instance represents.</param>
        inline const std::string value() const { return id; }

        /// Sets the ID of this instance.
        /// <param name="m">The WvW match ID this instance should
        /// represent.</param>
        inline void value(const std::string &m) { id = m; }

        const std::string str() const override { return id; }

    private:
        std::string id;
    };

    /// Less-comparator for WorldId objects.
    /// <returns>True if <c>lhs.value() &lt; rhs.value().</c>
    inline bool operator<(const WorldId &lhs, const WorldId &rhs) { return lhs.value() < rhs.value(); }

    /// Less-comparator for MapId objects.
    /// <returns>True if <c>lhs.value() &lt; rhs.value().</c>
    inline bool operator<(const MapId &lhs, const MapId &rhs) { return lhs.value() < rhs.value(); }

    /// Less-comparator for EventId objects.
    /// <returns>True if <c>lhs.value() &lt; rhs.value().</c>
    inline bool operator<(const EventId &lhs, const EventId &rhs) { return lhs.value() < rhs.value(); }

    /// Less-comparator for WvWMatchId objects.
    /// <returns>True if <c>lhs.value() &lt; rhs.value().</c>
    inline bool operator<(const WvWMatchId &lhs, const WvWMatchId &rhs) { return lhs.value() < rhs.value(); }

    /// Equality-comparator for WorldId objects.
    /// <returns>True if <c>lhs.value() == rhs.value().</c>
    inline bool operator==(const WorldId &lhs, const WorldId &rhs) { return lhs.value() == rhs.value(); }

    /// Equality-comparator for MapId objects.
    /// <returns>True if <c>lhs.value() == rhs.value().</c>
    inline bool operator==(const MapId &lhs, const MapId &rhs) { return lhs.value() == rhs.value(); }

    /// Equality-comparator for EventId objects.
    /// <returns>True if <c>lhs.value() == rhs.value().</c>
    inline bool operator==(const EventId &lhs, const EventId &rhs) { return lhs.value() == rhs.value(); }

    /// Equality-comparator for WvWMatchId objects.
    /// <returns>True if <c>lhs.value() == rhs.value().</c>
    inline bool operator==(const WvWMatchId &lhs, const WvWMatchId &rhs) { return lhs.value() == rhs.value(); }

    inline std::ostream& operator<<(std::ostream &os, const IdBase &id) { return os << id.str(); }
}

// Define hashers for above classes.
namespace std
{
    /// Hash specialization for gw2::MapId.
    template<>
    struct hash<gw2::MapId>
    {
        typedef size_t result_type;
        typedef gw2::MapId argument_type;

        /// The hash of the specified MapId instance, which is delegated to
        /// hash<int>.
        size_t operator()(const gw2::MapId &m) const;
    };

    /// Hash specialization for gw2::WorldId.
    template<>
    struct hash<gw2::WorldId>
    {
        typedef size_t result_type;
        typedef gw2::WorldId argument_type;

        /// The hash of the specified MapId instance, which is delegated to
        /// hash<int>.
        size_t operator()(const gw2::WorldId &w) const;
    };

    /// Hash specialization for gw2::EventId.
    template<>
    struct hash<gw2::EventId>
    {
        typedef size_t result_type;
        typedef gw2::EventId argument_type;

        /// The hash of the specified EventId instance, which is delegated to
        /// hash<std::string>.
        size_t operator()(const gw2::EventId &e) const;
    };

    /// Hash specialization for gw2::WvWMatchId.
    template<>
    struct hash<gw2::WvWMatchId>
    {
        typedef size_t result_type;
        typedef gw2::WvWMatchId argument_type;

        /// The hash of the specified WvWMatchId instance, which is delegated
        /// to hash<std::string>.
        size_t operator()(const gw2::WvWMatchId &e) const;
    };
}
#endif // GW2_WORLDID_H