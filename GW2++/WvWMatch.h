#ifndef GW2_WVWMATCH_H
#define GW2_WVWMATCH_H

#ifndef GW2_WORLDID_H
#include "WorldId.h"
#endif

#include <string>

namespace gw2
{
    class WvWMatch
    {
    public:
        WvWMatch(const WvWMatchId &id);
        
        WvWMatchId id() const { return match_id; }
        WorldId red_world() const { return red_id; }
        WorldId blue_world() const { return blue_id; }
        WorldId green_world() const { return green_id; }
        std::string start() const { return start_time; }
        std::string end() const { return end_time; }

        void id(const WvWMatchId &id) { match_id = id; }
        void red_world(const WorldId &world) { red_id = world; }
        void blue_world(const WorldId &world) { blue_id = world; }
        void green_world(const WorldId &world) { green_id = world; }
        void start(const std::string &time) { start_time = time; }
        void end(const std::string &time) { end_time = time; }

    private:
        WvWMatchId match_id;
        WorldId red_id;
        WorldId blue_id;
        WorldId green_id;
        std::string start_time;
        std::string end_time;
    };
}
#endif // GW2_WVWMATCH_H