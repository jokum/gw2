#ifndef GW2_APIBASE_H
#define GW2_APIBASE_H

#ifndef GW2_CERTIFICATE_H
#include "Certificate.h"
#endif

#ifndef GW2_CURLEASY_H
#include "CurlEasy.h"
#endif

#ifndef GW2_QUERY_H
#include "Query.h"
#endif

class JSONNode; // Forward declaration.

namespace gw2
{
    namespace api
    {
        /// Enum wrapper for API languages, in alphabetical order.
        /// Alternative to C++11's enum class.
        namespace Language
        {
            /// The API language name. APIs are built around the language
            /// selected during instantiation. Although the language is
            /// technically a query argument (see <see>gw2::Query</see>) it is
            /// not treated as one and cannot be changed at runtime.
            enum Name { English, French, German, Spanish };
        }

        /// Enum wrapper for supported APIs, in alphabetical order. Beta APIs
        /// will not be supported.
        namespace Path
        {
            enum Name
            {
                Events,
                EventNames,
                MapNames,
                WorldNames,
                WvWMatchDetails,
                WvWMatches,
                WvWObjectives
            };
        }

        /// Returns the fully qualified URL of a given API for a given
        /// language. <c>name</c> and <c>lang</c> are referenced in
        /// unordered_maps containing the corresponding string values.
        const std::string url(const api::Path::Name name, const api::Language::Name lang);

        /// This class represents the basis of any API, with a single important
        /// exception: this class does not provide access to any data fetched.
        /// It is the responsibility of deriving classes to provide access to
        /// fetched data themselves, ideally via a const T data() const member
        /// function. This is because a uniform T across all APIs is not always
        /// sensible.
        /// This class takes care of connecting, downloading data, and JSON
        /// parsing. It is necessary for deriving classes to implement the
        /// <c>void add_node(const JSONNode &node)</c> function, since T is not
        /// known.
        class ApiBase
        {
        public:
            virtual ~ApiBase() {}

            /// Establish a new connection to the API represented by this
            /// object, along with any arguments set with <c>set_query()</c>,
            /// download it, and start parsing it. The connection is
            /// subsequently closed again. This is a blocking operation.
            void refresh();

            /// Specify a set of query arguments to be made to the API this
            /// object represents. Queries are persistent across calls to
            /// <see cref="refresh"/>. The only way to set or reset a query is
            /// to define a new one.
            /// <param name="query">The query to execute against the API this
            /// object represents.</param>
            /// <see cref="gw2::Query"/>
            void set_query(const Query &query);

        protected:
            /// Constructs a new API for <c>name</c> in <c>language</c>, using
            /// an in-memory <see cref="Certificate"/> to authenticate the SSL
            /// connection.
            /// <param name="name">The <see cref="api::Path::Name"/> of this
            /// API.</param>
            /// <param name="language">The <see cref="api::Language::Name/> of
            /// this API.</param>
            ApiBase(const Path::Name name, const Language::Name language);

            /// Constructs a new API for <c>name</c> in <c>language</c>, using
            /// <c>cert</c> to authenticate the SSL connection.
            /// <param name="name">The <see cref="api::Path::Name"/> of this
            /// API.</param>
            /// <param name="cert">The <see cref="Certificate"/> with which to
            /// authenticate future connections made to this API.</param>
            /// <param name="language">The <see cref="api::Language::Name/> of
            /// this API.</param>
            ApiBase(const Path::Name name, const Certificate &cert, const Language::Name language);

        private:
            /// Wrapper for various CURL options.
            void prepare_curl();

            /// Returns the final URL to which this API connects during calls
            /// to <see cref="refresh"/>, including any query arguments.
            const std::string url() const;

            /// Parses incoming JSON data. This function does not add parsed
            /// data to any containers but instead delegates that
            /// responsibility to <see cref="add_node"/> which is declared
            /// virtual. This function is recursive.
            /// <param name="node">The JSON node to parse, recursively.</param>
            void parse(const JSONNode &node);

            /// Handles any preprocessing necessary for the data container.
            /// This function is called during every call to
            /// <see cref="refresh"/> In general this should probably be left
            /// as-is but when using for instance a vector calling <c>clear</c>
            /// here could make sense.
            virtual void prepare_container() {}

            /// Adds the specified node to the container chosen by the deriving
            /// class. No parsing is done here. See <see cref="parse"/>.
            /// <param name="node">The JSON node to process and add to the data
            /// container.</param>
            virtual void add_node(const JSONNode &node) = 0;

            Language::Name lang;
            Certificate certify;
            std::string base_url;
            std::string json_response;
            Query q;
            CurlEasy c;
        };
    }
}
#endif // GW2_APIBASE_H