#ifndef GW2_WVWMATCH_H
#include "WvWMatch.h"
#endif

namespace gw2
{
    WvWMatch::WvWMatch(const WvWMatchId &id)
        : match_id(id), red_id(), blue_id(), green_id(), start_time(), end_time()
    {
    }
}