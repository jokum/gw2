#ifndef GW2_EVENTS_H
#include "Events.h"
#endif

#ifndef JSON_OPTIONS_H
#include "../libjson/JSONOptions.h"
#endif

#ifndef LIBJSON_H
#include "../libjson/libjson.h"
#endif

#include <iostream>

namespace gw2
{
    namespace api
    {
        Events::Events()
            : ApiBase(api::Path::Events, api::Language::English) {}

        Events::Events(const Certificate &cert)
            : ApiBase(api::Path::Events, cert, api::Language::English) {}

        const std::vector<Event> Events::data() const
        {
            return events;
        }

        void Events::add_node(const JSONNode &node)
        {
            try
            {
                auto id = node.at("event_id").as_string(),
                    state = node.at("state").as_string();
                int world = std::stoi(node.at("world_id").as_string()),
                    map = std::stoi(node.at("map_id").as_string());

                events.emplace_back(Event(
                    EventId(id)
                    , str_to_state(state)
                    , WorldId(world)
                    , MapId(map)
                    ));
            }
            catch (std::logic_error &e)
            {
                // at() can throw out_of_range.
                // stoi() can throw out_of_range or invalid_argument.
                // There's no reason to treat them differently.
                std::cerr << "Failed to add node: " << node.as_string()
                    << ", " << e.what() << std::endl;
            }
        }

        void Events::refresh_specific(Event &e)
        {
            Query new_query(Query::Argument::EventId, e.id());
            new_query += Query(Query::Argument::WorldId, e.world_id());
            set_query(new_query);
            refresh();
            if (events.size())
            {
                e.state(events.at(0).state());
            }
        }

        void Events::prepare_container()
        {
            events.clear();
        }
    }
}