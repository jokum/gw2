#ifndef GW2_EVENT_H
#define GW2_EVENT_H

#ifndef GW2_WORLDID_H
#include "WorldId.h"
#endif

#include <string>

namespace gw2
{
    /// A representation of a Guild Wars 2 dynamic event.
    /// A dynamic event is uniquely identified by the <c>EventId</c> describing
    /// the event and the <c>WorldId</c> for the world the event instance
    /// belongs to. Every event exists on every world.
    class Event
    {
    public:
        /// The possible states of a dynamic event, ordered alphabetically.
        /// There is no strict order of progress between the individual states
        /// and an event is not required to go through all states.
        enum State { Active, Fail, Inactive, Preparation, Success, Warmup };

        /// Constructs a new event with default arguments such that the event
        /// is inactive and belongs nowhere. This makes the event invalid and
        /// it becomes necessary to later define at least the world ID.
        Event(const EventId &event_id, State event_state = State::Inactive
            , WorldId world_id = WorldId(0), MapId map_id = MapId(0));

        /// Constructs a new event with the given name with default arguments
        /// such that the event is inactive and belongs nowhere. This makes the
        /// event invalid and it becomes necessary to later define at least the
        /// world ID.
        Event(const EventId &event_id, const std::string &event_name = ""
            , State event_state = State::Inactive, WorldId world_id = WorldId(0), MapId map_id = MapId(0));

        /// Returns the GUID representing this event. Events are uniquely
        /// identified by an <c>EventId, WorldId</c> pair.
        inline const EventId id() const { return m_id; }

        /// Returns the name of this event.
        inline const std::string name() const { return m_name; }

        /// Returns the last known state of this event.
        /// <seealso>Event::State</seealso>
        inline const Event::State state() const { return m_state; }

        /// Returns the ID of the world this event instance belongs to.
        /// Events are uniquely identified by an <c>EventId, WorldId</c> pair.
        inline const WorldId world_id() const { return m_world_id; }

        /// Returns the ID of the map this event takes place in.
        inline const MapId map_id() const { return m_map_id; }

        /// Sets this event's name to <c>new_name</c>.
        inline void name(const std::string &new_name) { m_name = new_name; }

        /// Sets this event's state to <c>state</c>.
        inline void state(const Event::State state) { m_state = state; }

        /// Sets the ID of this event's world to <c>world</c>.
        inline void world_id(const WorldId &world) { m_world_id = world; }

        /// Sets the ID of the map this event takes place in to <c>map</c>.
        inline void map_id(const MapId &map) { m_map_id = map; }

    private:
        EventId m_id;
        std::string m_name;
        State m_state;
        WorldId m_world_id;
        MapId m_map_id;
    };

    /// Looks up <c>s</c> in an unordered_map of events and return the
    /// corresponding <c>Event::State</c>.
    /// <seealso>gw2::state_to_str()</seealso>
    const Event::State str_to_state(const std::string &s);

    /// Looks up the <c>std::string</c> representation of <c>state</c> in an
    /// unordered_map.
    /// <seealso>gw2::str_to_state()</seealso>
    const std::string state_to_str(const Event::State state);
}
#endif // GW2_EVENT_H