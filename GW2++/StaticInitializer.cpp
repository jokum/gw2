#ifndef GW2_STATICINITIALIZER_H
#include "StaticInitializer.h"
#endif

#ifndef __CURL_CURL_H
#include "curl/curl.h"
#endif

namespace gw2
{
    bool StaticInitializer::initialized(false);
    bool StaticInitializer::cleaned(false);

    StaticInitializer::StaticInitializer(void)
    {
        if (!StaticInitializer::initialized)
        {
            StaticInitializer::initialized = true;
            auto res = curl_global_init(CURL_GLOBAL_DEFAULT);
            if (res != CURLE_OK)
            {
                StaticInitializer::initialized = false;
            }
        }
    }

    StaticInitializer::~StaticInitializer(void)
    {
        if (StaticInitializer::initialized && !StaticInitializer::cleaned)
        {
            StaticInitializer::cleaned = true;
            curl_global_cleanup();
        }
    }
}